import socket
import threading
import P5


class ScoringGauntlet(threading.Thread):
    def __init__(self, host, port):
        threading.Thread.__init__(self)
        self.host = host
        self.port = port
        self.keepRunning = True
        self.descriptor = None
        self.stream = None

    def connect(self):
        # this is actually a "virtual" method if we had C/C++/Java at our disposal
        # we expect that self.stream is something that read / write / close can use
        print('Attempting to connect')
        try:
            self.descriptor = socket.create_connection((self.host, self.port))
            self.stream = self.descriptor.makefile("rw")
            self.keepRunning = True
        except IOError as blerg_io:
            print('Blerg (closing): ' + str(blerg_io.args))
            self.keepRunning = False
        return self.keepRunning

    def readln(self):
        return self.stream.readline()

    def writeln(self, command):
        self.descriptor.send(command.encode('utf-8', errors='ignore'))
        return

    def run(self):
        P5.gauntlet_loop(self)
        return

    def stop(self):
        self.keepRunning = False
        return

