import getopt
import subprocess
import sys
from time import time

import GameStatus
import Scoreboard
import SoundPlayer
from ScoringGauntlet import ScoringGauntlet

# Prototype of automated score keeper.
# 2021-02-10: Removing accelerometer and replace with "send", "lead" and "lead lost" buttons

args = {}

#    Right bits:
#    0: star pass
#    1: new pass / "send"
#    2: calloff
#    3: Star removed / ineligible for lead
#    5: lead declared (this button tracks when the finger is down, not up)

rindex_lead = 0
rindex_send = 1
rindex_starpass = 2
rindex_lost = 3

right_active = '0'
# in case we change our minds, but we're not performing inversion on the right hand set of buttons
# anymore
speaking_process = None


def gauntlet_loop(gauntlet):
    global speaking_process
    if gauntlet.connect():
        SoundPlayer.SoundPlayer(['24-Online.wav']).start()
        # speaking_process = subprocess.Popen(["mplayer", f"{GameStatus.sound_path}24-Online.wav"])
    else:
        SoundPlayer.SoundPlayer(['25-Unable.wav']).start()
        exit(1)

    # left-bits right-bits
    # 1111 000000
    # Quick reference:
    #    Left bits:
    #    0: Score buttons
    #    Right bits:
    #    0: star pass
    #    1: new pass / "send"
    #    2: calloff
    #    3: Star removed / ineligible for lead
    #    5: lead declared (this button tracks when the finger is down, not up)

    send_already_pushed = False
    score_already_recorded = False
    score_first_reported = 0
    star_pass_announced = False

    score_ignore = False
    # used for situations where Lost is pressed and then send is pressed to advance the trip

    lead_last_state = False

    # While the buttons track different states (eg. left hand buttons are "1" when the switch is not pressed, but
    # the right hand buttons are 1 if the buttons ARE pressed) they both mean the same things - 1 means "on"
    last_print = time()
    last_calloff = 0
    # this only needs to stop us from paying attention to the call off button from just after the first time it is
    # pressed until when the scoreboard broadcasts that the jam is off.

    while gauntlet.keepRunning:
        line = gauntlet.readln().strip()
        fields = line.replace('  ', ' ').split(' ')

        if len(fields) < 2 or fields[0][0] == '#':
            continue

        right_hand_buttons = fields[1]
        right_hand_buttons = right_hand_buttons[::-1]
        while len(right_hand_buttons) < 8:
            right_hand_buttons = f'{right_hand_buttons}0'

        if right_hand_buttons[rindex_starpass] == right_active and GameStatus.jam_on:
            print('Star-pass button pressed')
            if not star_pass_announced:
                print('Announcing star-pass')
                star_pass_announced = True
                GameStatus.lead_currently = False
                GameStatus.lead_available = False
                SoundPlayer.SoundPlayer([GameStatus.colour_wav, "06-StarPass.wav"]).start()
                gauntlet.writeln(f'b0\n')
                Scoreboard.star_pass()
        else:
            star_pass_announced = False

        # points processing:
        if GameStatus.jam_on:
            if right_hand_buttons[rindex_send] == right_active:
                if GameStatus.initial_trip:
                    speaking_process = SoundPlayer.SoundPlayer([GameStatus.colour_wav, "26-InitialComplete.wav"])
                    speaking_process.start()
                    Scoreboard.initial_complete()
                    send_already_pushed = True
                    score_ignore = True
                elif not score_ignore:
                    print('Points hand is up')
                    points_field = fields[0]
                    score = points_field.count('1')
                    if not send_already_pushed:
                        print('\treporting score')
                        send_already_pushed = True
                        score_first_reported = score
                        score_already_recorded = True
                        speaking_process = SoundPlayer.SoundPlayer([GameStatus.colour_wav, f"0{score + 1}-0{score}.wav"])
                        speaking_process.start()
                        Scoreboard.score(score)
                    else:
                        if score_first_reported != score:
                            print('\tScore corrected')
                            if speaking_process is not None:
                                speaking_process.stop()
                            speaking_process =  SoundPlayer.SoundPlayer(["23-Correction.wav", GameStatus.colour_wav,f"0{score + 1}-0{score}.wav"])
                            speaking_process.start()
                            score_first_reported = score
                            Scoreboard.score(score)
            else:
                # the hand went up, now the hand went down. Start the next scoring trip
                if score_already_recorded:
                    print('buzzing')
                    gauntlet.writeln(f'b{score_first_reported}\n')
                    Scoreboard.trip_over()
                score_ignore = score_already_recorded = send_already_pushed = False
                speaking_process = None
        else:
            # Feedback - adding points is needed after a jam ends
            if right_hand_buttons[rindex_send] == right_active:
                if not score_ignore:
                    print('Points hand is up')
                    points_field = fields[0]
                    score = points_field.count('1')
                    if not send_already_pushed:
                        print('\treporting score')
                        send_already_pushed = True
                        score_first_reported = score
                        score_already_recorded = True
                        speaking_process = SoundPlayer.SoundPlayer([GameStatus.colour_wav, f"0{score + 1}-0{score}.wav"])
                        speaking_process.start()
                        Scoreboard.score(score)
                    else:
                        if score_first_reported != score:
                            print('\tScore corrected')
                            if speaking_process is not None:
                                speaking_process.stop()
                            speaking_process = SoundPlayer.SoundPlayer(["23-Correction.wav", GameStatus.colour_wav, f"0{score + 1}-0{score}.wav"])
                            speaking_process.start()
                            score_first_reported = score
                            Scoreboard.score(score)
            else:
                if score_already_recorded:
                    print('buzzing')
                    gauntlet.writeln(f'b{score_first_reported}\n')
                    Scoreboard.trip_over()
                score_ignore = score_already_recorded = send_already_pushed = False
                speaking_process = None

        # lead processing:
        # we mainly use Gamestatus.jam_on for lead - points we can do between jams.
        if right_hand_buttons[rindex_lead] != right_active:
            # someone has let go of the lead button that means they're pointing their finger, presumably to signal lead
            if GameStatus.jam_on:
                # only react to the rising edge (i.e. lead_last_state from False to True)
                # This way a JR that hasn't pressed the button down in time still has an
                # opportunity to do so without messing up the system. This does however mean the
                # JR will need to "click" the button if they don't press it at least once after
                # the jam start
                if not lead_last_state: # we are going from negative to positive
                    # awarding lead can only happen on initial trip:
                    if GameStatus.initial_trip and GameStatus.lead_available:
                        print('Lead indicated... reporting')
                        GameStatus.lead_us = True
                        GameStatus.lead_currently = True
                        GameStatus.lead_available = False
                        gauntlet.writeln('b2\n')
                        speaking_process = SoundPlayer.SoundPlayer([GameStatus.colour_wav, f"07-Lead.wav"])
                        speaking_process.start()
                        Scoreboard.lead_award()
                        lead_last_state = True
            else:
                lead_last_state = True
        else:
            lead_last_state = False
            if GameStatus.jam_on:
                # if the jammer is lead, and someone presses the lead button again, we assume they are calling the jam
                # thereby merging 2 buttons and getting the button count down to 4
                if GameStatus.lead_currently and (time() - last_calloff) > 5:
                    Scoreboard.calloff()
                    last_calloff = time()
                    print('\tCalling')

        # however, losing lead lasts forever...
        if right_hand_buttons[rindex_lost] == right_active and GameStatus.jam_on:
            if not GameStatus.lead_lost_reported:
                print('Lost indicated...')
                speaking_process = SoundPlayer.SoundPlayer([GameStatus.colour_wav, "08-LostLead.wav"])
                speaking_process.start()
                Scoreboard.lead_lost()
                GameStatus.lead_currently = False
                gauntlet.writeln('b0\n')
                GameStatus.lead_available = False
                GameStatus.lead_lost_reported = True


if __name__ == '__main__':
    # Command-line arguments are:
    # g: hostname (of Gauntlet) - assumes port 23 ala ESP-01S esp-link.
    # b: scoreboard URL eg. ws://localhost:8000/WS/
    # t: team number in CRG-ese (i.e. 1 or 2)
    opt_list, arg_list = getopt.getopt(sys.argv[1:], "d:g:b:t:f")
    # print(opt_list)
    for item in opt_list:
        key = item[0].replace("-", "")
        for the_arg in item[1:]:
            if key in args:
                args[key].append(the_arg)
            else:
                args[key] = [the_arg]
    GameStatus.sound_path = './'
    if 'd' in args:
        GameStatus.sound_path = args['d'][0]
    if not GameStatus.sound_path.endswith('/'):
        GameStatus.sound_path += '/'

    if 'f' in args:
        # f means "flip the buttons" - i.e. we are wearing the buttons upside down
        rindex_lead = 3
        rindex_send = 2
        rindex_starpass = 1
        rindex_lost = 0
    if 'g' not in args:
        args['g'] = ['192.168.0.41']
    if 'b' in args:
        # scoreBoard url - if missing, no scoreboard interlink
        if 't' in args:
            # team number (1 or 2) - if missing no SB interlink
            GameStatus.team_number = int(args['t'][0])
            Scoreboard.scoreboard_start(args['b'][0])
        else:
            print('Not enough scoreboard connection information')
            exit(1)
    else:
        print('Not enough scoreboard connection information')
        exit(1)

    gauntlet_loop(ScoringGauntlet(args['g'][0], 23))
    exit(0)
