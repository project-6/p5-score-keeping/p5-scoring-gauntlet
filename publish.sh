curl -D- --request POST --header "PRIVATE-TOKEN: $TOKENTOKEN" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/tags?tag_name=${CI_PIPELINE_ID}-${CI_COMMIT_SHORT_SHA}-$1&ref=master"
curl -D- --header "Content-Type: application/json" \
  --header "JOB-TOKEN: $CI_JOB_TOKEN" \
  --data "{ \"name\": \"$1 release\", \"tag_name\": \"${CI_PIPELINE_ID}-${CI_COMMIT_SHORT_SHA}-$1\", \"ref\": \"$CI_COMMIT_SHORT_SHA\", \"description\": \"$1 PyInstaller\", \"assets\": { \"links\": [{ \"name\": \"$1 PyInstaller\", \"url\": \"${CI_PROJECT_URL}/-/jobs/$CI_JOB_ID/artifacts/raw/dist/$2\" }] } }" \
  --request POST "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases"
