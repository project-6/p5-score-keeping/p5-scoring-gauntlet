#!/usr/bin/env python3
import json
import os
import sys
import tkinter as tk
import tkinter.messagebox
from pathlib import Path
import GameStatus
import P5
import Scoreboard
from ScoringGauntlet import ScoringGauntlet


class Application(tk.Frame):
    def __init__(self, master=None):
        self.gauntlet_connection = None

        tk.Frame.__init__(self, master)

        row = 0
        self.main_label = tk.Label(self, text='JR Gauntlet Control')
        self.main_label.grid(column=0, columnspan=4, row=row, sticky='wens')
        row += 1

        self.scoreboard_address_label = tk.Label(self, text='Scoreboard Address:')
        self.scoreboard_address_label.grid(column=0, row=row, sticky='w')
        self.scoreboard_address_text = tk.Text(self, height=1, width=20)
        self.scoreboard_address_text.grid(column=1, row=row, sticky='we')
        self.scoreboard_connect_button = tk.Button(self, text="Connect To Scoreboard", command=self.scoreboard_connect)
        self.scoreboard_connect_button.grid(column=2, row=row, sticky='we')
        row += 1

        self.gauntlet_address_label = tk.Label(self, text='Gauntlet Address:')
        self.gauntlet_address_label.grid(column=0, row=row, sticky='w')
        self.gauntlet_address_text = tk.Text(self, height=1, width=20)
        self.gauntlet_address_text.grid(column=1, row=row, sticky='we')
        self.gauntlet_connect_button = tk.Button(self, text="Connect To Gauntlet", command=self.gauntlet_connect)
        self.gauntlet_connect_button.grid(column=2, row=row, sticky='we')
        row += 1

        self.stuff_frame = tk.Frame(self)
        self.stuff_frame.grid(column=0, columnspan=4, row=row, sticky='w')
        self.right_fingers = tk.StringVar()
        self.right_fingers.set("Vector")
        self.right_fingers_dropdown = tk.OptionMenu(self.stuff_frame, self.right_fingers,
                                            *["Vector", "Vector UD", "Rainbow", "Rainbow UD"],
                                            command=self.set_right_fingers)
        self.right_fingers_dropdown.grid(column=0, row=0, sticky='w')

        self.voice = tk.StringVar()
        self.voice.set("Voice-A")
        self.voice_label = tk.Label(self.stuff_frame, text='Voice:')
        self.voice_label.grid(column=1, row=0, sticky='w')
        self.voice_dropdown = tk.OptionMenu(self.stuff_frame, self.voice,
                                            *["Voice-A", "Voice-1", "Off"],
                                            command=self.change_voice)
        self.voice_dropdown.grid(column=1, row=0, sticky='w')
        self.team = tk.StringVar()
        self.team.set('Team-1')
        GameStatus.team_number = 1
        self.team_label = tk.Label(self.stuff_frame, text='Team:')
        self.team_label.grid(column=3, row=0, sticky='w')
        self.team_dropdown = tk.OptionMenu(self.stuff_frame, self.team,
                                            *["Team-1", "Team-2"],
                                            command=self.set_team)
        self.team_dropdown.grid(column=4, row=0, sticky='w')
        row += 1

        self.ops_frame = tk.Frame(self)
        self.ops_frame.grid(column=0, row=row, columnspan=4, sticky='wens')
        self.ops_frame.grid_rowconfigure(0, weight=1)
        self.ops_frame.grid_columnconfigure(0, weight=1)
        self.ops_frame.grid_columnconfigure(1, weight=1)
        self.ops_frame.grid_columnconfigure(2, weight=1)
        self.quitButton = tk.Button(self.ops_frame, text='Quit', command=self.end)
        self.quitButton.grid(column=0, row=0, sticky='ewns')
        self.saveButton = tk.Button(self.ops_frame, text='Save', command=self.save)
        self.saveButton.grid(column=1, row=0, sticky='ewns')
        self.pageButton = tk.Button(self.ops_frame, text='Page JR', command=self.page)
        self.pageButton.grid(column=2, row=0, sticky='ewns')
        self.aboutButton = tk.Button(self.ops_frame, text='About', command=self.about)
        self.aboutButton.grid(column=3, row=0, sticky='ewns')
        row += 1

        self.grid()
        self.scoreboard_connected = False
        self.beacon_connection = None
        self.gauntlet_connected = False

        self.load()
        if hasattr(sys, '_MEIPASS'):
            path = sys._MEIPASS
        else:
            path = './'
        GameStatus.sound_path = os.path.join(path, 'voices', 'voice-A')
        return

    def set_right_fingers(self, new_setting):
        if new_setting == 'Vector UD':
            P5.rindex_lead = 3
            P5.rindex_send = 2
            P5.rindex_starpass = 1
            P5.rindex_lost = 0
        elif new_setting == 'Vector':
            P5.rindex_lead = 0
            P5.rindex_send = 1
            P5.rindex_starpass = 2
            P5.rindex_lost = 3
        elif new_setting == 'Rainbow':
            P5.rindex_lead = 0
            P5.rindex_send = 1
            P5.rindex_lost = 2
            P5.rindex_starpass = 3
        else:
            P5.rindex_lead = 3
            P5.rindex_send = 2
            P5.rindex_lost = 1
            P5.rindex_starpass = 0

    def change_voice(self, newvoice):
        path = './'
        if hasattr(sys, '_MEIPASS'):
            path = sys._MEIPASS
        if newvoice == 'Voice-A':
            GameStatus.sound_path = os.path.join(path, 'voices', 'voice-A')
        elif newvoice == 'Voice-1':
            GameStatus.sound_path = os.path.join(path, 'voices', 'voice-1')
        else:
            # kludge: point it somewhere with no voices
            GameStatus.sound_path = os.path.join(path, 'voices', 'voice-none')
        return

    def scoreboard_connect(self):
        if not self.scoreboard_connected:
            # TODO: work out how to close scoreboard
            address = self.scoreboard_address_text.get("1.0", "end-1c")
            Scoreboard.scoreboard_start(f'ws://{address}/WS')
            self.scoreboard_connected = True
        else:
            global sb_connection
            Scoreboard.sb_connection.close()
            self.scoreboard_connected = False
        self.scoreboard_connect_button['text'] = \
            'Disconnect From Scoreboard' if self.scoreboard_connected else 'Connect To Scoreboard'
        return

    def gauntlet_connect(self):
        if self.gauntlet_connected:
            self.gauntlet_connection.stop()
            self.gauntlet_connected = False
        else:
            self.gauntlet_connection = ScoringGauntlet(f'{self.gauntlet_address_text.get("1.0", "end-1c")}', 23)
            self.gauntlet_connection.start()
            self.gauntlet_connected = True
        self.gauntlet_connect_button['text'] = \
            'Disconnect From Gauntlet' if self.gauntlet_connected else 'Connect To Gauntlet'
        return

    def end(self):
        if self.scoreboard_connected:
            Scoreboard.scoreboard_stop()
        if self.gauntlet_connected:
            self.gauntlet_connection.stop()
        self.quit()

    def page(self):
        self.gauntlet_connection.writeln('b0\n')
        return

    def save(self):
        save_object = {
            'gauntlet': self.gauntlet_address_text.get("1.0", "end-1c"),
            'scoreboard': self.scoreboard_address_text.get("1.0", "end-1c")
        }
        # homedir = Path.home()
        # homedir.
        file = Path.home().joinpath(".p5cfg").open("w+")
        json.dump(save_object, file)
        file.close()

    @staticmethod
    def set_text(obj, value):
        obj.delete("1.0", "end")
        obj.insert("1.0", value)

    def load(self):
        if not Path.home().joinpath(".p5cfg").exists():
            return
        file = Path.home().joinpath(".p5cfg").open("r")
        saved_object = json.load(file)
        file.close()
        if 'gauntlet' in saved_object:
            self.set_text(self.gauntlet_address_text, saved_object['gauntlet'])
        if 'scoreboard' in saved_object:
            self.set_text(self.scoreboard_address_text, saved_object['scoreboard'])
        if 'flip' in saved_object:
            self.flip_right.set(saved_object['flip'] > 0)
        return

    @staticmethod
    def about():
        tkinter.messagebox.showinfo("About JR Gauntlet", "JR Gauntlet by Vector (Western Australia Roller Derby)")

    @staticmethod
    def set_team(team):
        if team == 'Team-1':
            GameStatus.team_number = 1
        else:
            GameStatus.team_number = 2


if __name__ == "__main__":
    try:
        # if we are running inside a pyinstaller package, we display a splashscreen while we are
        # unzipping - and thus, we should close it once we get started
        import pyi_splash
        pyi_splash.close()
    except:
        pass

    app = Application()
    app.master.title('JR Gauntlet')
    # join with sys._MEIPASS is needed to access a file path inside a pyinstaller bundle:
    if hasattr(sys, '_MEIPASS'):
        app.master.iconphoto(True, tk.PhotoImage(file=os.path.join(sys._MEIPASS, 'icon.png')))
    else:
        app.master.iconphoto(True, tk.PhotoImage(file='icon.png'))
    app.mainloop()
    exit(0)
    # call_command = 'call'
