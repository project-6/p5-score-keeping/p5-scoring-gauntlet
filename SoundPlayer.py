import os
import threading
import playsound
import GameStatus


class SoundPlayer(threading.Thread):
    def __init__(self, list):
        threading.Thread.__init__(self)
        self.list = list
        self.keepRunning = True

    def stop(self):
        self.keepRunning = False

    def run(self):
        if 'none' in GameStatus.sound_path:
            return
        for file in self.list:
            if not self.keepRunning:
                break
            path = os.path.join(GameStatus.sound_path, file)
            playsound.playsound(path, block=True)
        return
