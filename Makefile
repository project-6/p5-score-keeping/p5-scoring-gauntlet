MKINSTALL=pyinstaller
all:  stuff

stuff: GUI.py GameStatus.py P5.py Scoreboard.py ScoringGauntlet.py icon.png
	$(MKINSTALL) GUI.py -d all --add-data="icon.png:." --add-data="voices/voice-A:voices/voice-A" --add-data "voices/voice-1:voices/voice-1" --splash icon.png --onefile -n P5-jrg 

clean:
	-rm -r dist/
	-rm *.pyo
